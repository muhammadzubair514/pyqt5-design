# Basic libraries to import for completing the whole work.
import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore


class Mainwindow(QMainWindow):
    def __init__(self):
        super(Mainwindow,self).__init__()
        loadUi("designform.ui",self) # Here we imported the QT Designer file which we made as Python GUI FIle.
        
        # Command to remove the default Windows Frame Design.
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        # Command to make the backgroud of Window transparent.
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        
        
app = QApplication(sys.argv)
window = Mainwindow()
window.show()
sys.exit(app.exec_())        
        